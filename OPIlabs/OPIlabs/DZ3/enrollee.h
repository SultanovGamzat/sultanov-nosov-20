#pragma once
#include <iostream>

using namespace std;
class Enrollee
{
public:
	int quantity = 0;
	string faculties[5];
	void application(string name);
	void print_faculties();
	void return_docs();
};